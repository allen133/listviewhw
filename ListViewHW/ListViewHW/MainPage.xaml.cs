﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ListViewHW.Classes;

namespace ListViewHW
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            MainListView.ItemsSource = new List<ListViewTemplate>
            {
                new ListViewTemplate
                {
                    Image = "Gorilla.jpg",
                    Name = "Gorilla",
                    Description = "Mammal",
                    Population = 900
                },
                new ListViewTemplate
                {
                    Image = "Leopard.jpg",
                    Name = "Amur Leopard",
                    Description = "Mammal",
                    Population = 70
                },
                new ListViewTemplate
                {
                    Image = "Turtle.jpg",
                    Name = "Sea Turtle",
                    Description = "Amphibian",
                    Population = 2000000
                },
                new ListViewTemplate
                {
                    Image = "Orangutan.jpg",
                    Name = "Orangutan",
                    Description = "Mammal",
                    Population = 6600
                },
                new ListViewTemplate
                {
                    Image = "Elephant.jpg",
                    Name = "Sumatran Elephant",
                    Description = "Mammal",
                    Population = 2000
                },
                new ListViewTemplate
                {
                    Image = "Saola.jpg",
                    Name = "Saola",
                    Description = "Mammal",
                    Population = 750
                },
                new ListViewTemplate
                {
                    Image = "Vaquita.jpg",
                    Name = "Vaquita",
                    Description = "Marinw Mammal",
                    Population = 100
                },
                new ListViewTemplate
                {
                    Image = "Tiger.jpg",
                    Name = "Tiger",
                    Description = "Mammal",
                    Population = 2000
                },
                new ListViewTemplate
                {
                    Image = "Rhino.jpg",
                    Name = "Rhino",
                    Description = "Mammal",
                    Population = 60
                },
                new ListViewTemplate
                {
                    Image = "Pangolin.jpg",
                    Name = "Pangolin",
                    Description = "Mammal",
                    Population = 1000000
                }
            };
        }

        public MainPage(string name, string des, string pop)
        {
            InitializeComponent();
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            MainListView.IsRefreshing = false;
        }
        //This is for open pages on the certain cells just requires one tap
        async void OpenView(object sender, SelectedItemChangedEventArgs e)
        {

            await Navigation.PushAsync(new InfoPage());
        }
        //This adds new object to the view requires a hold down
        async void AddClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddPage());
        }


    }
}
