﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ListViewHW.Classes
{
    class ListViewTemplate
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Population { get; set; }
        public string Image { get; set; }
    }
}
